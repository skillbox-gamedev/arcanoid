// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"

#include "Arcanoid/Base/BallBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASpawner::ASpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ChanceInPercent = 100;
	NotSpawnedCounter = 0;
	SpawnedCounter = 0;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	NotSpawnedCounter = FGenericPlatformMath::RoundToInt(100 / ChanceInPercent) - 1;
	SpawnedCounter = FGenericPlatformMath::RoundToInt(100 / ChanceInPercent) - NotSpawnedCounter;
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpawner::SpawnBonus(FVector Location)
{
	if (ShouldSpawn())
	{
		int32 RandomIndex = 0;
		if (SpawnActorArray.Num() > 1)
		{
			RandomIndex = FMath::RandRange(0, SpawnActorArray.Num() - 1);
		}
		if (const auto ClassToSpawn = SpawnActorArray[RandomIndex])
		{
			GetWorld()->SpawnActor<AActor>(ClassToSpawn, FTransform(Location));
		}
	}
}

void ASpawner::ActivateTimer(float Seconds)
{
	GetWorldTimerManager().SetTimer(BonusTimerHandler, this, &ASpawner::DestroyBonusEffect, Seconds, false);
}

void ASpawner::DestroyBonusEffect()
{
	TArray<AActor*> FoundBalls;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABallBase::StaticClass(), FoundBalls);
	for (const auto FoundBall : FoundBalls)
	{
		if (const ABallBase* Ball = Cast<ABallBase>(FoundBall))
		{
			Ball->MeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Block);
		}
	}
}

bool ASpawner::ShouldSpawn()
{
	if (SpawnedCounter == 0 && NotSpawnedCounter == 0)
	{
		NotSpawnedCounter = FGenericPlatformMath::RoundToInt(100 / ChanceInPercent) - 1;
		SpawnedCounter = FGenericPlatformMath::RoundToInt(100 / ChanceInPercent) - NotSpawnedCounter;
		return true;
	}
	if ((SpawnedCounter > 0 && FMath::RandBool()) || NotSpawnedCounter == 0)
	{
		SpawnedCounter--;
		return true;
	}
	NotSpawnedCounter--;
	return false;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlockBase.h"
#include "GameFramework/Actor.h"
#include "ArenaBuilder.generated.h"

UCLASS()
class ARCANOID_API AArenaBuilder : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AArenaBuilder();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Defaults")
	TArray<TSubclassOf<ABlockBase>> Blocks;

	UPROPERTY()
	int32 ArenaNumber;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

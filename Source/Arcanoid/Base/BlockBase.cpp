// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockBase.h"

#include "BallBase.h"
#include "NiagaraFunctionLibrary.h"
#include "Arcanoid/Utils/ColorUtils.h"

// Sets default values
ABlockBase::ABlockBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionObjectType(ECC_GameTraceChannel1);
	MeshComponent->SetNotifyRigidBodyCollision(true);
	Lives = 1;
	Score = 100;
}

// Called when the game starts or when spawned
void ABlockBase::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentHit.AddDynamic(this, &ABlockBase::HandleOnHit);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABlockBase::HandleOverlap);
}

// Called every frame
void ABlockBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABlockBase::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                             FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		if (Cast<ABallBase>(OtherActor))
		{
			if (this->Lives == 1)
			{
				const FVector BlockLocation = this->GetActorLocation();
				const FRotator Rotator(90, 0, 0);
				const FVector ParticleScale(0.35, 0.35, 0.35);
				OnDeadDelegate.Broadcast(this, this->GetActorLocation());
				this->Destroy();
				UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), DestroyParticle, BlockLocation, Rotator,
														   ParticleScale);
			}
			else
			{
				this->Lives--;
				ChangeMaterial();
			}
		}
	}
}

void ABlockBase::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                               UPrimitiveComponent* OtherComp,
                               int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		if (Cast<ABallBase>(OtherActor))
		{
			const FVector BlockLocation = this->GetActorLocation();
			const FRotator Rotator(0, 0, 0);
			const FVector ParticleScale(0.35, 0.35, 0.35);
			OnDeadDelegate.Broadcast(this, this->GetActorLocation());
			this->Destroy();
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), DestroyParticle, BlockLocation, Rotator,
														   ParticleScale);
		}
	}
}

void ABlockBase::ChangeMaterial()
{
	FVector ColorVector;
	if (Lives == 1)
	{
		ColorVector = ColorUtils::Green;
	}
	else if (Lives == 2)
	{
		ColorVector = ColorUtils::Yellow;	
	}
	else
	{
		ColorVector = ColorUtils::Red;
	}
	this->MeshComponent->SetVectorParameterValueOnMaterials("Color01", ColorVector);
	this->MeshComponent->SetVectorParameterValueOnMaterials("EmissiveColor", ColorVector);
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameWinWidget.generated.h"

class UTextBlock;
class UButton;
/**
 * 
 */
UCLASS()
class ARCANOID_API UGameWinWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Start;
	
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Quit;

	UPROPERTY()
	FString CurrentLevelName;

public:
	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UTextBlock* ScoreText;

	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UTextBlock* CongratulationsText;

	UPROPERTY()
	UWorld* World;
	
public:
	virtual bool Initialize() override;

	UFUNCTION()
	void OnStartClicked();
	UFUNCTION()
	void OnQuitClicked();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnAdditionalBall.h"

#include "Arcanoid/Base/PlatformBase.h"
#include "Arcanoid/Base/PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

void ASpawnAdditionalBall::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		if (const auto Platform = Cast<APlatformBase>(Actor); IsValid(Platform))
		{
			APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
			if (APlayerPawn* Pawn = Cast<APlayerPawn>(PlayerPawn))
			{
				Pawn->CreateBallActor(true);
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Orange,TEXT("Spawned additional ball"));
				}
			}
		}
	}
}

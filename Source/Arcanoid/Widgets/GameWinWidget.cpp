// Fill out your copyright notice in the Description page of Project Settings.


#include "GameWinWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

bool UGameWinWidget::Initialize()
{
	Super::Initialize();
	if (!Start || !Quit)
	{
		return false;
	}
	Start->OnClicked.AddDynamic(this, &UGameWinWidget::OnStartClicked);
	Quit->OnClicked.AddDynamic(this, &UGameWinWidget::OnQuitClicked);
	World = GetWorld();
	if (!World || !World->GetCurrentLevel())
	{
		return false;
	}
	CurrentLevelName = World->GetCurrentLevel()->GetOuter()->GetName();
	return true;
}

void UGameWinWidget::OnStartClicked()
{
	const FString LevelName = "ArcanoidGameMap";
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	UGameplayStatics::OpenLevel(World, *LevelName, true, "");
}

void UGameWinWidget::OnQuitClicked()
{
	const TEnumAsByte QuitPreference = EQuitPreference::Quit;
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), QuitPreference,
								   true);
}

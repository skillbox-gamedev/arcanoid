// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class ARCANOID_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Spawner")
	TArray<TSubclassOf<AActor>> SpawnActorArray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Spawner")
	float ChanceInPercent;

	UPROPERTY()
	int32 NotSpawnedCounter;

	UPROPERTY()
	int32 SpawnedCounter;
	
	FTimerHandle BonusTimerHandler;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void SpawnBonus(FVector Location);
	void ActivateTimer(float Seconds);
	void DestroyBonusEffect();
	bool ShouldSpawn();
};

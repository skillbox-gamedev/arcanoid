// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

class UTextBlock;
class UButton;

UCLASS()
class ARCANOID_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Start;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Quit;

public:
	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UTextBlock* ScoreText;

	UPROPERTY(VisibleAnywhere, meta=(BindWidget))
	UTextBlock* CongratulationsText;

	UPROPERTY()
	UWorld* World;
	
public:
	virtual bool Initialize() override;

	UFUNCTION()
	void OnStartClicked();
	UFUNCTION()
	void OnQuitClicked();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "IgnoreHitBall.h"

#include "Spawner.h"
#include "Arcanoid/Base/PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

AIgnoreHitBall::AIgnoreHitBall()
{
	TimerRate = 5.0f;
}

void AIgnoreHitBall::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		TArray<AActor*> FoundBalls;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABallBase::StaticClass(), FoundBalls);
		for (const auto FoundBall : FoundBalls)
		{
			if (const ABallBase* Ball = Cast<ABallBase>(FoundBall))
			{
				Ball->MeshComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Overlap);
			}
		}

		if (const APlayerPawn* Pawn = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0)))
		{
			ASpawner* Spawner = Pawn->SpawnerActor;
			Spawner->ActivateTimer(5.0f);
		}
	}
}

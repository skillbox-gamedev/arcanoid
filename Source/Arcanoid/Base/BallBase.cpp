// Fill out your copyright notice in the Description page of Project Settings.


#include "BallBase.h"

#include <string>

#include "GameFramework/KillZVolume.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/AudioComponent.h"

// Sets default values
ABallBase::ABallBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetNotifyRigidBodyCollision(true);
	MoveDirection = FVector(1.0, 1.0, 0.0);
	bCanMove = false;
	MaxBallSpeed = 1500;
	MaxBallSpeed = 500;
	SetupAudioComponent();
}

// Called when the game starts or when spawned
void ABallBase::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABallBase::OverlapBegin);
	MeshComponent->OnComponentHit.AddDynamic(this, &ABallBase::HandleOnHit);
	AudioComponent->SetSound(HitSound);
}

// Called every frame
void ABallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (bCanMove)
	{
		Move(DeltaTime);
	}
}

void ABallBase::Move(float DeltaTime)
{
	FVector NewLocation = MoveDirection * Speed * DeltaTime;
	NewLocation.Z = 0.0f;
	FHitResult HitResult;
	const FRotator Rotator(NewLocation.X * -1, NewLocation.Y * -1, NewLocation.Z * -1);
	AddActorWorldRotation(Rotator);
	AddActorWorldOffset(NewLocation, true, &HitResult);
	if (HitResult.IsValidBlockingHit())
	{
		const FVector NormalVector = HitResult.Normal;
		MoveDirection = FMath::GetReflectionVector(MoveDirection, NormalVector);
	}
}

void ABallBase::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                             UPrimitiveComponent* OtherComp,
                             int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		if (Cast<AKillZVolume>(OtherActor))
		{
			const FVector BallLocation = this->GetActorLocation();
			const FRotator Rotator(0, 0, 0);
			const FVector ParticleScale(0.35, 0.35, 0.35);
			OnDeadDelegate.Broadcast();
			this->Destroy();
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), HitParticles, BallLocation, Rotator,
			                                               ParticleScale);
			AudioComponent->SetSound(DieSound);
			AudioComponent->Play();
		}
	}
}

void ABallBase::HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                            FVector NormalImpulse, const FHitResult& Hit)
{
	AudioComponent->Play();
}

void ABallBase::SetupAudioComponent()
{
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->bAutoActivate = false;
}

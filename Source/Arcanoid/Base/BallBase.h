// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BallBase.generated.h"

class UNiagaraSystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeadDelegate);

UCLASS()
class ARCANOID_API ABallBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABallBase();

	UPROPERTY(EditDefaultsOnly)
	double Speed;

	UPROPERTY(EditDefaultsOnly)
	FVector MoveDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(BlueprintReadWrite)
	bool bCanMove;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	double MaxBallSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	double MinBallSpeed;

	UPROPERTY(BlueprintAssignable)
	FDeadDelegate OnDeadDelegate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VFX")
	UNiagaraSystem* HitParticles;

	UPROPERTY()
	UAudioComponent* AudioComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundWave* HitSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USoundWave* DieSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Move(float DeltaTime);
	UFUNCTION()
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                  int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void HandleOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                 FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void SetupAudioComponent();
};

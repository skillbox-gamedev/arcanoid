// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BallBase.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

class AArenaBuilder;
enum class EDifficult;
class USaveGameData;
class UNiagaraSystem;
class UGameWinWidget;
class UGameOverWidget;
class UPauseMenuWidget;
class UPlayerWidget;
class UMainMenuWidget;
class UCameraComponent;
class APlatformBase;
class ABlockBase;
class ASpawner;

UCLASS()
class ARCANOID_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	FTimerHandle HardModeTimerHandler;
	
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	APlatformBase* PlatformActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APlatformBase> PlatformActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABallBase> BallActorClass;

	UPROPERTY(BlueprintReadWrite)
	ABallBase* BallActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABlockBase> BlockActorClass;

	UPROPERTY(BlueprintReadWrite)
	ABlockBase* BlockActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpawner> SpawnerClass;

	UPROPERTY(BlueprintReadWrite)
	ASpawner* SpawnerActor;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AArenaBuilder> ArenaBuilderClass;

	UPROPERTY(BlueprintReadWrite)
	AArenaBuilder* ArenaBuilder;
	
	UPROPERTY(VisibleAnywhere)
	int32 BallActorsCount;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UPlayerWidget> PlayerWidgetClass;

	UPROPERTY()
	UPlayerWidget* PlayerWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UMainMenuWidget> MainMenuWidgetClass;

	UPROPERTY()
	UMainMenuWidget* MainMenuWidget;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UPauseMenuWidget> PauseMenuWidgetClass;

	UPROPERTY()
	UPauseMenuWidget* PauseMenuWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameOverWidget> GameOverWidgetClass;

	UPROPERTY()
	UGameOverWidget* GameOverWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameWinWidget> GameWinWidgetClass;

	UPROPERTY()
	UGameWinWidget* GameWinWidget;
	
	UPROPERTY()
	int32 Lives;

	UPROPERTY()
	int32 Score;

	UPROPERTY()
	int32 BlocksCount;

	UPROPERTY()
	TArray<AActor*> Blocks;

	UPROPERTY()
	EDifficult Difficult;

	UPROPERTY(EditAnywhere)
	float SecondsToMoveBlocks;

	UPROPERTY()
	UAudioComponent* MusicComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="DefaultSettings")
	USoundWave* Music;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DefaultSettings")
	float MusicVolume;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	void CreatePlatformActor();
	void CreateBallActor(bool bCanMove = false);
	UFUNCTION()
	void HandlePlayerInput(float Value);
	UFUNCTION()
	void StartGame();
	UFUNCTION()
	void WhenBallDestroyed();
	UFUNCTION()
	void ConstructArena();
	UFUNCTION()
	void OnBlockDestroyed(ABlockBase* Block, FVector Location);
	void CreateSpawner();
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void Pause();
	void GameOver();
	void WinGame();
	void ActivateHardMode(float Seconds);
	void MoveBlocks();
	UFUNCTION()
	void PlayMusic();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Arcanoid/Base/ArenaBuilder.h"
#include "Arcanoid/Base/BlockBase.h"

/**
 * 
 */
class ARCANOID_API ArenaGenerator
{
public:
	ArenaGenerator();
	~ArenaGenerator();
	inline static int32 Padding = 123;
	static int32 GenerateRandom(UWorld* World, AArenaBuilder* Builder);
	static void GenerateByNumber(UWorld* World, AArenaBuilder* Builder, int32 ArenaNumber);
private:
	static void FullFilled(UWorld* World, AArenaBuilder* Builder);
	static void OddOrEvenColumn(UWorld* World, AArenaBuilder* Builder, bool bIsOdd = true);
	static void OddOrEvenRow(UWorld* World, AArenaBuilder* Builder, bool bIsOdd = true);
	static void AcrossOne(UWorld* World, AArenaBuilder* Builder);
	static void SpawnBlock(UWorld* World, TSubclassOf<ABlockBase> BlockClass, const FVector& Location);
	static TSubclassOf<ABlockBase> GetRandomBlock(AArenaBuilder* Builder);
};

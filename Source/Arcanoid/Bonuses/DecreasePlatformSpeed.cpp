// Fill out your copyright notice in the Description page of Project Settings.


#include "DecreasePlatformSpeed.h"

#include "Arcanoid/Base/PlatformBase.h"

ADecreasePlatformSpeed::ADecreasePlatformSpeed()
{
	DecreaseValue = 0.5f;
}

void ADecreasePlatformSpeed::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		const auto Platform = Cast<APlatformBase>(Actor);
		if (IsValid(Platform))
		{
			float PlatformSpeed = Platform->Speed;

			if (PlatformSpeed > Platform->MinSpeed)
			{
				PlatformSpeed -= DecreaseValue;
			}
			Platform->Speed = PlatformSpeed;
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red,
												 FString::Format(*FString(TEXT("Decrease Platform Speed, now is: {0}")),
																 {Platform->Speed}));
			}
		}
	}
}

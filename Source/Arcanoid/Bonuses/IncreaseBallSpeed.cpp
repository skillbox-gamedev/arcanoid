// Fill out your copyright notice in the Description page of Project Settings.


#include "IncreaseBallSpeed.h"

#include "Arcanoid/Base/BallBase.h"
#include "Kismet/GameplayStatics.h"

AIncreaseBallSpeed::AIncreaseBallSpeed()
{
	IncreaseValue = 100.0f;
}

void AIncreaseBallSpeed::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		TArray<AActor*> FoundBalls;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABallBase::StaticClass(), FoundBalls);
		for (AActor* BallActor : FoundBalls)
		{
			if (ABallBase* Ball = Cast<ABallBase>(BallActor); IsValid(Ball))
			{
				float BallSpeed = Ball->Speed;

				if (BallSpeed < Ball->MaxBallSpeed)
				{
					BallSpeed += IncreaseValue;
				}
				Ball->Speed = BallSpeed;
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green,
													 FString::Format(*FString(TEXT("Increase Ball Speed, now is: {0}")),
																	 {Ball->Speed}));
				}
			}
		}
	}
}

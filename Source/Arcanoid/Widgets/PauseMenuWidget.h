// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

class UButton;

UCLASS()
class ARCANOID_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Resume;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Retry;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Quit;

	UPROPERTY()
	FString CurrentLevelName;

	UPROPERTY()
	UWorld* World;
	
public:
	virtual bool Initialize() override;

	UFUNCTION()
	void OnResumeClicked();
	UFUNCTION()
	void OnRetryClicked();
	UFUNCTION()
	void OnQuitClicked();
};

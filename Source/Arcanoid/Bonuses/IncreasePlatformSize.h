// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "IncreasePlatformSize.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AIncreasePlatformSize : public ABonusBase
{
	GENERATED_BODY()

public:
	AIncreasePlatformSize();
	
	UPROPERTY(EditDefaultsOnly)
	double IncreaseValue;
	
	void Apply(AActor* Actor) override;
	void CalculateNewLocation(float NewYScale, FVector& CurrentLocation);
};

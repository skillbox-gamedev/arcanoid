// Fill out your copyright notice in the Description page of Project Settings.


#include "PauseMenuWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

bool UPauseMenuWidget::Initialize()
{
	Super::Initialize();
	Resume->OnClicked.AddDynamic(this, &UPauseMenuWidget::OnResumeClicked);
	Retry->OnClicked.AddDynamic(this, &UPauseMenuWidget::OnRetryClicked);
	Quit->OnClicked.AddDynamic(this, &UPauseMenuWidget::OnQuitClicked);
	World = GetWorld();
	if (!World->GetCurrentLevel())
	{
		return false;
	}
	CurrentLevelName = World->GetCurrentLevel()->GetOuter()->GetName();
	return true;
}

void UPauseMenuWidget::OnResumeClicked()
{
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	this->RemoveFromParent();
	UGameplayStatics::SetGamePaused(World, false);
}

void UPauseMenuWidget::OnRetryClicked()
{
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	UGameplayStatics::OpenLevel(World, FName(CurrentLevelName), true, "");
}

void UPauseMenuWidget::OnQuitClicked()
{
	const TEnumAsByte QuitPreference = EQuitPreference::Quit;
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), QuitPreference,
								   true);
}

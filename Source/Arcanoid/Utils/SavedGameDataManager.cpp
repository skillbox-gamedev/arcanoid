#include "SavedGameDataManager.h"

#include "Arcanoid/SaveGameData/SaveGameData.h"
#include "Kismet/GameplayStatics.h"

USaveGameData* SavedGameDataManager::SaveGame(int32 RecordScore, int32 LastArenaNumber, EDifficult SaveDifficult)
{
	USaveGameData* SaveGameData = Cast<USaveGameData>(
		UGameplayStatics::CreateSaveGameObject(USaveGameData::StaticClass()));
	SaveGameData->RecordScore = RecordScore;
	SaveGameData->LastArenaNumber = LastArenaNumber;
	SaveGameData->Difficult = SaveDifficult;
	UGameplayStatics::SaveGameToSlot(SaveGameData, SaveGameData->SlotName, 0);
	return SaveGameData;
}

USaveGameData* SavedGameDataManager::LoadGame()
{
	USaveGameData* SaveGameData = Cast<USaveGameData>(UGameplayStatics::CreateSaveGameObject(USaveGameData::StaticClass()));
	if (const USaveGame* SavedGame = UGameplayStatics::LoadGameFromSlot(SaveGameData->SlotName, 0); !SavedGame)
	{
		UGameplayStatics::SaveGameToSlot(SaveGameData, SaveGameData->SlotName, 0);
	}
	else
	{
		SaveGameData = Cast<USaveGameData>(UGameplayStatics::LoadGameFromSlot(SaveGameData->SlotName, 0));
	}
	return SaveGameData;
}

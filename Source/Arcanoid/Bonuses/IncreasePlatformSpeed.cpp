// Fill out your copyright notice in the Description page of Project Settings.


#include "IncreasePlatformSpeed.h"

#include "Arcanoid/Base/PlatformBase.h"

AIncreasePlatformSpeed::AIncreasePlatformSpeed()
{
	IncreaseValue = 0.5f;
}

void AIncreasePlatformSpeed::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		if (const auto Platform = Cast<APlatformBase>(Actor); IsValid(Platform))
		{
			float PlatformSpeed = Platform->Speed;

			if (PlatformSpeed <= 50.0f)
			{
				PlatformSpeed += IncreaseValue;
			}
			Platform->Speed = PlatformSpeed;
			if (GEngine)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green,
												 FString::Format(*FString(TEXT("Increase Platform Speed, now is: {0}")),
																 {Platform->Speed}));
			}
		}
	}
}

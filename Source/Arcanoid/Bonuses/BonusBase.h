// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Arcanoid/Interfaces/Applicable.h"
#include "Arcanoid/Interfaces/Movable.h"
#include "GameFramework/Actor.h"
#include "BonusBase.generated.h"

UCLASS()
class ARCANOID_API ABonusBase : public AActor, public IApplicable, public IMovable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="BonusSettings")
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly, Category="BonusSettings")
	double Speed;

	UPROPERTY(EditDefaultsOnly, Category="BonusSettings")
	FVector MoveDirection;

	UPROPERTY()
	UAudioComponent* AudioComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="BonusSettings")
	USoundWave* EatSound;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Apply(AActor* Actor) override;
	void Move(float DeltaTime) override;

	UFUNCTION()
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
					  UPrimitiveComponent* OtherComp,
					  int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};

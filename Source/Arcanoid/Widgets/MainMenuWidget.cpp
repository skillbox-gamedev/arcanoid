// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"

#include "Arcanoid/SaveGameData/SaveGameData.h"
#include "Arcanoid/Utils/SavedGameDataManager.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

bool UMainMenuWidget::Initialize()
{
	Super::Initialize();
	Start->OnClicked.AddDynamic(this, &UMainMenuWidget::OnStartClicked);
	Quit->OnClicked.AddDynamic(this, &UMainMenuWidget::OnQuitClicked);
	HardMode->OnClicked.AddDynamic(this, &UMainMenuWidget::OnHardModeClicked);
	World = GetWorld();
	if (!World->GetCurrentLevel())
	{
		return false;
	}
	CurrentLevelName = World->GetCurrentLevel()->GetOuter()->GetName();
	StartLevelName = "ArcanoidGameMap";
	return true;
}

void UMainMenuWidget::OnStartClicked()
{
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	UGameplayStatics::OpenLevel(World, *StartLevelName, true, "");
}

void UMainMenuWidget::OnHardModeClicked()
{
	const USaveGameData* SaveGameData = SavedGameDataManager::LoadGame();
	SavedGameDataManager::SaveGame(SaveGameData->RecordScore, SaveGameData->LastArenaNumber, EDifficult::HARD);
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	UGameplayStatics::OpenLevel(World, *StartLevelName, true, "");
}

void UMainMenuWidget::OnQuitClicked()
{
	const TEnumAsByte QuitPreference = EQuitPreference::Quit;
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), QuitPreference,
	                               true);
}

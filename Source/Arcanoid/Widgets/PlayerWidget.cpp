// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerWidget.h"

#include <string>

#include "Components/Image.h"
#include "Components/TextBlock.h"

void UPlayerWidget::DecreaseLives(int32 Lives)
{
	UImage* Image = Lives == 2 ? LiveThree : Lives == 1 ? LiveTwo : LiveOne;
	Image->SetColorAndOpacity(FLinearColor::Black);
}

void UPlayerWidget::IncreaseScore(int32 Scores)
{
	if (Score)
	{
		const int CurrentScore = FCString::Atoi(*Score->GetText().ToString());
		Score->SetText(FText::FromString(FString::Format(*FString(TEXT("{0}")), {CurrentScore + Scores})));
	}
}

void UPlayerWidget::SetRecordScore(int32 RecordScores)
{
	if (RecordScore)
	{
		RecordScore->SetText(FText::FromString(FString::Format(*FString(TEXT("{0}")), {RecordScores})));
	}
}

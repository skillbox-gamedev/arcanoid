// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"

#include "Arcanoid/Base/PlatformBase.h"
#include "Components/AudioComponent.h"
#include "GameFramework/KillZVolume.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetNotifyRigidBodyCollision(true);
	MeshComponent->SetCollisionObjectType(ECC_GameTraceChannel1);
	MeshComponent->SetCollisionProfileName(FName(TEXT("OverlapAllDynamic")));
	MoveDirection = FVector(-1, 0, 0);
	Speed = 600;
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->bAutoActivate = false;
}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABonusBase::OverlapBegin);
	AudioComponent->SetSound(EatSound);
	EatSound->Volume = 1.0f;
}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);
}

void ABonusBase::Apply(AActor* Actor)
{
	IApplicable::Apply(Actor);
}

void ABonusBase::Move(float DeltaTime)
{
	const FVector NewLocation = MoveDirection * Speed * DeltaTime;
	AddActorWorldOffset(NewLocation, true);
}

void ABonusBase::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		if (Cast<AKillZVolume>(OtherActor))
		{
			this->Destroy();
		}
		if (APlatformBase* PlatformBase = Cast<APlatformBase>(OtherActor))
		{
			Apply(PlatformBase);
			this->Destroy();
			AudioComponent->Play();
		}
	}
}


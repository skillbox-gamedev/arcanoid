// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

bool UGameOverWidget::Initialize()
{
	Super::Initialize();
	Start->OnClicked.AddDynamic(this, &UGameOverWidget::OnStartClicked);
	Quit->OnClicked.AddDynamic(this, &UGameOverWidget::OnQuitClicked);
	World = GetWorld();
	if (!World->GetCurrentLevel())
	{
		return false;
	}
	return true;
}

void UGameOverWidget::OnStartClicked()
{
	const FString LevelName = "ArcanoidGameMap";
	UGameplayStatics::GetPlayerController(World, 0)->SetInputMode(FInputModeGameOnly());
	UGameplayStatics::OpenLevel(World, *LevelName, true, "");
}

void UGameOverWidget::OnQuitClicked()
{
	const TEnumAsByte QuitPreference = EQuitPreference::Quit;
	UKismetSystemLibrary::QuitGame(World, UGameplayStatics::GetPlayerController(World, 0), QuitPreference,
								   true);
}

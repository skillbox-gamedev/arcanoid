// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaGenerator.h"

ArenaGenerator::ArenaGenerator()
{
}

ArenaGenerator::~ArenaGenerator()
{
}

int32 ArenaGenerator::GenerateRandom(UWorld* World, AArenaBuilder* Builder)
{
	switch (FMath::RandRange(1, 6))
	{
	case 1:
		FullFilled(World, Builder);
		return 1;
	case 2:
		OddOrEvenColumn(World, Builder, false);
		return 2;
	case 3:
		OddOrEvenRow(World, Builder);
		return 3;
	case 4:
		AcrossOne(World, Builder);
		return 4;
	case 5:
		OddOrEvenRow(World, Builder, false);
		return 5;
	case 6:
		OddOrEvenColumn(World, Builder);
		return 6;
	default:
		FullFilled(World, Builder);
		return 1;
	}
}

void ArenaGenerator::GenerateByNumber(UWorld* World, AArenaBuilder* Builder, int32 ArenaNumber)
{
	switch (ArenaNumber)
	{
	case 1:
		FullFilled(World, Builder);
		break;
	case 2:
		OddOrEvenColumn(World, Builder, false);
		break;
	case 3:
		OddOrEvenRow(World, Builder);
		break;
	case 4:
		AcrossOne(World, Builder);
		break;
	case 5:
		OddOrEvenRow(World, Builder, false);
		break;
	case 6:
		OddOrEvenColumn(World, Builder);
		break;
	default:
		FullFilled(World, Builder);
		break;
	}
}

void ArenaGenerator::FullFilled(UWorld* World, AArenaBuilder* Builder)
{
	constexpr float StartY = -930;
	for (int x = 0; x <= 5; x++)
	{
		for (int y = 0; y <= 15; y++)
		{
			const auto Block = GetRandomBlock(Builder);
			constexpr float StartX = 930;
			const float InX = StartX - x * Padding;
			const float InY = y * Padding + StartY;
			const FVector Location(InX, InY, 20);
			SpawnBlock(World, Block, Location);
		}
	}
}

void ArenaGenerator::OddOrEvenColumn(UWorld* World, AArenaBuilder* Builder, bool bIsOdd)
{
	constexpr float StartY = -930;

	for (int x = 0; x <= 5; x++)
	{
		for (int y = bIsOdd; y <= 15 + bIsOdd; y = y + 2)
		{
			const auto Block = GetRandomBlock(Builder);
			constexpr float StartX = 930;
			const float InX = StartX - x * Padding;
			const float InY = y * Padding + StartY;
			const FVector Location(InX, InY, 20);
			SpawnBlock(World, Block, Location);
		}
	}
}

void ArenaGenerator::OddOrEvenRow(UWorld* World, AArenaBuilder* Builder, bool bIsOdd)
{
	constexpr float StartY = -930;

	for (int x = bIsOdd; x <= 5 + bIsOdd; x = x + 2)
	{
		for (int y = 0; y <= 15; y++)
		{
			const auto Block = GetRandomBlock(Builder);
			constexpr float StartX = 930;
			const float InX = StartX - x * Padding;
			const float InY = y * Padding + StartY;
			const FVector Location(InX, InY, 20);
			SpawnBlock(World, Block, Location);
		}
	}
}

void ArenaGenerator::AcrossOne(UWorld* World, AArenaBuilder* Builder)
{
	constexpr float StartYAxis = -930;
	int StartY;
	for (int x = 0; x <= 6; x++)
	{
		if (x == 0 || x % 2 == 0)
		{
			StartY = 0;
		}
		else
		{
			StartY = 1;
		}
		for (int y = StartY; y <= 15; y = y + 2)
		{
			const auto Block = GetRandomBlock(Builder);
			constexpr float StartX = 930;
			const float InX = StartX - x * Padding;
			const float InY = y * Padding + StartYAxis;
			const FVector Location(InX, InY, 20);
			SpawnBlock(World, Block, Location);
		}
	}
}

void ArenaGenerator::SpawnBlock(UWorld* World, TSubclassOf<ABlockBase> BlockClass, const FVector& Location)
{
	const FRotator Rotator(0, 0, 0);
	ABlockBase* BlockBase = World->SpawnActor<ABlockBase>(BlockClass, Location, Rotator, FActorSpawnParameters());
	const int RandomLives = FMath::RandRange(1, 3);
	BlockBase->Lives = RandomLives;
	BlockBase->Score = BlockBase->Score * RandomLives;
	BlockBase->SetActorScale3D(FVector(2.0f));
	BlockBase->ChangeMaterial();
}

TSubclassOf<ABlockBase> ArenaGenerator::GetRandomBlock(AArenaBuilder* Builder)
{
	return Builder->Blocks[FMath::RandRange(0, Builder->Blocks.Num() - 1)];
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "DecreasePlatformSize.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API ADecreasePlatformSize : public ABonusBase
{
	GENERATED_BODY()

public:
	ADecreasePlatformSize();
	
	UPROPERTY(EditDefaultsOnly)
	double DecreaseValue;
	
	void Apply(AActor* Actor) override;
};

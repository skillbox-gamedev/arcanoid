// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "DecreasePlatformSpeed.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API ADecreasePlatformSpeed : public ABonusBase
{
	GENERATED_BODY()

public:
	ADecreasePlatformSpeed();
	
	UPROPERTY(EditDefaultsOnly)
	double DecreaseValue;
	
	void Apply(AActor* Actor) override;
};

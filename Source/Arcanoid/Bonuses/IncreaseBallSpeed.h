// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "IncreaseBallSpeed.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AIncreaseBallSpeed : public ABonusBase
{
	GENERATED_BODY()

public:
	AIncreaseBallSpeed();
	
	UPROPERTY(EditDefaultsOnly)
	double IncreaseValue;
	
	void Apply(AActor* Actor) override;
};

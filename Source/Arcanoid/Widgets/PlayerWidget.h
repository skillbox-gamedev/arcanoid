// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerWidget.generated.h"

class UTextBlock;
class UImage;

UCLASS()
class ARCANOID_API UPlayerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* LiveOne;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* LiveTwo;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* LiveThree;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UTextBlock* Score;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UTextBlock* RecordScore;
	
public:
	void DecreaseLives(int32 Lives);
	void IncreaseScore(int32 Scores);
	void SetRecordScore(int32 RecordScores);
};

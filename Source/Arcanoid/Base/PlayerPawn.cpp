// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"

#include "Arcanoid/Utils/ArenaGenerator.h"
#include "Arcanoid/Widgets/GameOverWidget.h"
#include "Arcanoid/Widgets/GameWinWidget.h"
#include "Arcanoid/Widgets/MainMenuWidget.h"
#include "Arcanoid/Widgets/PauseMenuWidget.h"
#include "PlatformBase.h"
#include "PlayerControllerBase.h"
#include "ArenaBuilder.h"
#include "Arcanoid/Widgets/PlayerWidget.h"
#include "Arcanoid/Bonuses/Spawner.h"
#include "Arcanoid/SaveGameData/SaveGameData.h"
#include "Arcanoid/Utils/SavedGameDataManager.h"
#include "Components/AudioComponent.h"
#include "Components/EditableText.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	BallActorsCount = 0;
	PlayerWidgetClass = nullptr;
	PlayerWidget = nullptr;
	MainMenuWidget = nullptr;
	MainMenuWidgetClass = nullptr;
	PauseMenuWidget = nullptr;
	PauseMenuWidgetClass = nullptr;
	Lives = 3;
	SecondsToMoveBlocks = 20;
	MusicComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("Music"));
	MusicComponent->bAutoActivate = false;
	MusicVolume = 0.5f;
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	if (IsLocallyControlled() && PlayerWidgetClass)
	{
		APlayerControllerBase* PlayerController = GetController<APlayerControllerBase>();
		PlayerWidget = CreateWidget<UPlayerWidget>(PlayerController, PlayerWidgetClass);
		PlayerWidget->AddToPlayerScreen();
		const int32 RecordScore = SavedGameDataManager::LoadGame()->RecordScore;
		PlayerWidget->SetRecordScore(RecordScore);
	}
	SetActorRotation(FRotator(-90, 0, 0));
	CreatePlatformActor();
	CreateBallActor();
	ConstructArena();
	CreateSpawner();
	PlayMusic();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveSide", this, &APlayerPawn::HandlePlayerInput);
	PlayerInputComponent->BindAction("StartGame", IE_Pressed, this, &APlayerPawn::StartGame);
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &APlayerPawn::Pause);
}

void APlayerPawn::CreatePlatformActor()
{
	const FVector Location(-480, 0, 50);
	const FRotator Rotator(0, 0, 0);
	PlatformActor = GetWorld()->SpawnActor<APlatformBase>(PlatformActorClass, Location, Rotator,
	                                                      FActorSpawnParameters());
	PlatformActor->CapsuleComponent->SetRelativeLocation(FVector(10.f, 0.f, 50.0f));
}

void APlayerPawn::CreateBallActor(bool bCanMove)
{
	const float InX = PlatformActor->CapsuleComponent->GetComponentToWorld().GetLocation().X + 60;
	const float InY = PlatformActor->CapsuleComponent->GetComponentToWorld().GetLocation().Y;
	const FVector Location(InX, InY, 50);
	const FRotator Rotator(0, 0, 0);
	BallActor = GetWorld()->SpawnActor<ABallBase>(BallActorClass, Location, Rotator, FActorSpawnParameters());
	BallActor->bCanMove = bCanMove;
	if (!bCanMove)
	{
		BallActor->AttachToActor(PlatformActor, FAttachmentTransformRules::KeepWorldTransform);
	}
	BallActor->OnDeadDelegate.AddDynamic(this, &APlayerPawn::WhenBallDestroyed);
	BallActorsCount++;
}

void APlayerPawn::HandlePlayerInput(float Value)
{
	if (IsValid(PlatformActor))
	{
		const float InY = Value * PlatformActor->Speed;
		PlatformActor->AddActorWorldOffset(FVector(0, InY, 0), true);
	}
}

void APlayerPawn::StartGame()
{
	if (IsValid(BallActor) && BallActor->bCanMove == false)
	{
		BallActor->bCanMove = true;
		BallActor->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		if (Difficult == EDifficult::HARD)
		{
			ActivateHardMode(SecondsToMoveBlocks);
		}
	}
}

void APlayerPawn::WhenBallDestroyed()
{
	if (BallActorsCount <= 1)
	{
		CreateBallActor(true);
		Lives--;
		PlayerWidget->DecreaseLives(Lives);
		if (Lives == 0)
		{
			GameOver();
		}
	}
	BallActorsCount--;
}

void APlayerPawn::ConstructArena()
{
	const FVector Location(1020, 0, 40);
	const FRotator Rotator(0, 0, 0);
	ArenaBuilder = GetWorld()->SpawnActor<AArenaBuilder>(ArenaBuilderClass, Location, Rotator,
	                                                     FActorSpawnParameters());
	const USaveGameData* GameData = SavedGameDataManager::LoadGame();
	if (const int32 SavedArenaNumber = GameData->LastArenaNumber; SavedArenaNumber != 0)
	{
		ArenaBuilder->ArenaNumber = SavedArenaNumber;
		ArenaGenerator::GenerateByNumber(GetWorld(), ArenaBuilder, ArenaBuilder->ArenaNumber);
	}
	else
	{
		ArenaBuilder->ArenaNumber = ArenaGenerator::GenerateRandom(GetWorld(), ArenaBuilder);
		SavedGameDataManager::SaveGame(GameData->RecordScore, ArenaBuilder->ArenaNumber, GameData->Difficult);
	}
	Difficult = GameData->Difficult;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABlockBase::StaticClass(), Blocks);
	BlocksCount = Blocks.Num();
}

void APlayerPawn::OnBlockDestroyed(ABlockBase* Block, FVector Location)
{
	const int BlockScore = Block->Score;
	PlayerWidget->IncreaseScore(BlockScore);
	Score += BlockScore;
	BlocksCount--;
	if (BlocksCount == 0)
	{
		WinGame();
	}
	SpawnerActor->SpawnBonus(Location);
}

void APlayerPawn::CreateSpawner()
{
	const FVector Location(1020, 0, 40);
	const FRotator Rotator(0, 0, 0);
	SpawnerActor = GetWorld()->SpawnActor<ASpawner>(SpawnerClass, Location, Rotator,
	                                                FActorSpawnParameters());
	TArray<AActor*> FoundBlocks;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABlockBase::StaticClass(), FoundBlocks);
	for (AActor* FoundBlock : FoundBlocks)
	{
		ABlockBase* Block = Cast<ABlockBase>(FoundBlock);
		if (Block)
		{
			Block->OnDeadDelegate.AddDynamic(this, &APlayerPawn::OnBlockDestroyed);
		}
	}
}

void APlayerPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	const USaveGameData* GameData = SavedGameDataManager::LoadGame();
	if (EndPlayReason == EEndPlayReason::Quit || EndPlayReason == EEndPlayReason::EndPlayInEditor)
	{
		int32 SavedRecordScore = GameData->RecordScore;
		if (Score > SavedRecordScore)
		{
			SavedRecordScore = Score;
		}
		SavedGameDataManager::SaveGame(SavedRecordScore, 0, EDifficult::EASY);
		if (PlayerWidget)
		{
			PlayerWidget->RemoveFromParent();
			PlayerWidget = nullptr;
		}
	}
	Super::EndPlay(EndPlayReason);
}

void APlayerPawn::Pause()
{
	APlayerControllerBase* PlayerController = GetController<APlayerControllerBase>();
	PauseMenuWidget = CreateWidget<UPauseMenuWidget>(PlayerController, PauseMenuWidgetClass);
	PauseMenuWidget->AddToPlayerScreen();
	PlayerController->SetInputMode(FInputModeUIOnly());
	PlayerController->SetShowMouseCursor(true);
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

void APlayerPawn::GameOver()
{
	APlayerControllerBase* PlayerController = GetController<APlayerControllerBase>();
	GameOverWidget = CreateWidget<UGameOverWidget>(PlayerController, GameOverWidgetClass);

	const int32 RecordScore = SavedGameDataManager::LoadGame()->RecordScore;
	if (Score > RecordScore)
	{
		GameOverWidget->CongratulationsText->SetVisibility(ESlateVisibility::Visible);
		SavedGameDataManager::SaveGame(RecordScore, 0, EDifficult::EASY);
	}
	GameOverWidget->ScoreText->SetText(FText::FromString(FString::FromInt(Score)));
	GameOverWidget->AddToPlayerScreen();
	PlayerController->SetInputMode(FInputModeUIOnly());
	PlayerController->SetShowMouseCursor(true);
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

void APlayerPawn::WinGame()
{
	APlayerControllerBase* PlayerController = GetController<APlayerControllerBase>();
	GameWinWidget = CreateWidget<UGameWinWidget>(PlayerController, GameWinWidgetClass);

	const int32 RecordScore = SavedGameDataManager::LoadGame()->RecordScore;
	if (Score > RecordScore)
	{
		GameWinWidget->CongratulationsText->SetVisibility(ESlateVisibility::Visible);
		SavedGameDataManager::SaveGame(RecordScore, 0, Difficult);
	}
	GameWinWidget->ScoreText->SetText(FText::FromString(FString::FromInt(Score)));

	GameWinWidget->AddToPlayerScreen();
	PlayerController->SetInputMode(FInputModeUIOnly());
	PlayerController->SetShowMouseCursor(true);
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

void APlayerPawn::ActivateHardMode(float Seconds)
{
	GetWorldTimerManager().SetTimer(HardModeTimerHandler, this, &APlayerPawn::MoveBlocks, Seconds, true);
}


void APlayerPawn::MoveBlocks()
{
	for (const auto FoundBlock : Blocks)
	{
		if (FoundBlock)
		{
			const FVector CurrentLocation = GetTargetLocation(FoundBlock);
			const FVector NewLocation = FVector((CurrentLocation.X - ArenaGenerator::Padding) * -1, 0, 0);
			FoundBlock->AddActorWorldOffset(NewLocation, true);
		}
	}
}

void APlayerPawn::PlayMusic()
{
	Music->bLooping = true;
	Music->Volume = MusicVolume;
	MusicComponent->SetSound(Music);
	MusicComponent->Play();
}

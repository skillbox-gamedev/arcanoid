// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "IncreasePlatformSpeed.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AIncreasePlatformSpeed : public ABonusBase
{
	GENERATED_BODY()

public:
	AIncreasePlatformSpeed();
	
	UPROPERTY(EditDefaultsOnly)
	double IncreaseValue;
	
	void Apply(AActor* Actor) override;
};

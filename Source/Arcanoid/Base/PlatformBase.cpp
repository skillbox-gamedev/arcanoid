// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformBase.h"

// Sets default values
APlatformBase::APlatformBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshCoponent"));
	MeshComponent->SetupAttachment(RootComponent);
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollision"));
	CapsuleComponent->SetupAttachment(MeshComponent);
	Speed = 10.0f;
	MinWidth = 3.0f;
	MaxWidth = 10.0f;
	ScaleX = 0.4f;
	ScaleZ = 0.4f;
	MaxSpeed = 20.0f;
	MinSpeed = 5.0f;
}

// Called when the game starts or when spawned
void APlatformBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlatformBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

class UButton;
class UEditableText;

UCLASS()
class ARCANOID_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Start;

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* HardMode;
	
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UButton* Quit;

	UPROPERTY()
	FString CurrentLevelName;

	UPROPERTY()
	UWorld* World;

	UPROPERTY()
	FString StartLevelName;
	
public:
	virtual bool Initialize() override;

	UFUNCTION()
	void OnStartClicked();
	UFUNCTION()
	void OnHardModeClicked();
	UFUNCTION()
	void OnQuitClicked();
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "DecreaseBallSpeed.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API ADecreaseBallSpeed : public ABonusBase
{
	GENERATED_BODY()

public:
	ADecreaseBallSpeed();
	
	UPROPERTY(EditDefaultsOnly)
	double DecreaseValue;
	
	void Apply(AActor* Actor) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class ARCANOID_API ColorUtils
{
public:
	ColorUtils();
	~ColorUtils();

	inline static FVector Green = FVector(0, 0.556863, 0);
	inline static FVector Yellow = FVector(1, 0.976471, 0);
	inline static FVector Red = FVector(1, 0, 0);
};

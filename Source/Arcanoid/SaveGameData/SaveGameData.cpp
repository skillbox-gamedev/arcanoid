// Fill out your copyright notice in the Description page of Project Settings.


#include "SaveGameData.h"

USaveGameData::USaveGameData()
{
	SlotName = TEXT("SaveSlot");
	RecordScore = 0;
	LastArenaNumber = 0;
	Difficult = EDifficult::EASY;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SaveGameData.generated.h"

UENUM()
enum class EDifficult
{
	EASY,
	HARD
};

UCLASS()
class ARCANOID_API USaveGameData : public USaveGame
{
	GENERATED_BODY()

public:

	USaveGameData();

	UPROPERTY()
	FString SlotName;

	UPROPERTY(VisibleAnywhere)
	int32 RecordScore;

	UPROPERTY()
	int32 LastArenaNumber;

	UPROPERTY()
	EDifficult Difficult;
	
};

#pragma once

class USaveGameData;
enum class EDifficult;

class SavedGameDataManager
{
public:
	static USaveGameData* SaveGame(int32 RecordScore, int32 LastArenaNumber, EDifficult SaveDifficult);
	static USaveGameData* LoadGame();
};

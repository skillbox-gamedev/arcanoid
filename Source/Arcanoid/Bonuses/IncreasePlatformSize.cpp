// Fill out your copyright notice in the Description page of Project Settings.


#include "IncreasePlatformSize.h"

#include "Arcanoid/Base/PlatformBase.h"

AIncreasePlatformSize::AIncreasePlatformSize()
{
	IncreaseValue = 0.5f;
}

void AIncreasePlatformSize::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		const auto Platform = Cast<APlatformBase>(Actor);
		if (IsValid(Platform))
		{
			const FVector CurrentScale = Platform->GetActorScale3D();
			if (CurrentScale.Y < Platform->MaxWidth)
			{
				const float NewYScale = CurrentScale.Y + IncreaseValue;
				Platform->SetActorScale3D(FVector(Platform->ScaleX, NewYScale, Platform->ScaleZ));
				const float CurrentCapsuleHalfHeight = Platform->CapsuleComponent->GetUnscaledCapsuleHalfHeight();
				const float NewCapsuleHalfHeight = CurrentCapsuleHalfHeight + 4;
				Platform->CapsuleComponent->SetCapsuleHalfHeight(NewCapsuleHalfHeight);

				FVector CurrentLocation = Platform->GetActorLocation();
				CalculateNewLocation(NewYScale, CurrentLocation);
				Platform->SetActorLocation(CurrentLocation);
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange,
													 FString::Format(
														 *FString(TEXT("Increase Platform Size, now is: {0}")),
														 {Platform->GetActorScale3D().Y}));

					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange,
													 FString::Format(
														 *FString(TEXT("Increase Capsule Size, now is: {0}")),
														 {Platform->CapsuleComponent->GetComponentScale().Y}));
				}
			}
		}
	}
}

void AIncreasePlatformSize::CalculateNewLocation(float NewYScale, FVector& CurrentLocation)
{
	/*
	 * Тут нужно для динамических расчётов (если применимо) получать координаты левой или правой стены.
	 * У меня на карте это Y = 1020
	 */
	const float DeltaPosition = (1020 - FMath::Abs(CurrentLocation.Y)) / NewYScale;
	/*
	 * 48 - возможный заезд платформы "в стену"
	 */
	if (DeltaPosition < 48)
	{
		if (CurrentLocation.Y > 0)
		{
			CurrentLocation.Y = CurrentLocation.Y - 48;
		}
		else
		{
			CurrentLocation.Y = CurrentLocation.Y + 48;
		}
	}
}

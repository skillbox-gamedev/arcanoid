// Fill out your copyright notice in the Description page of Project Settings.


#include "DecreasePlatformSize.h"

#include "Arcanoid/Base/PlatformBase.h"

ADecreasePlatformSize::ADecreasePlatformSize()
{
	DecreaseValue = 0.5f;
}

void ADecreasePlatformSize::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		if (const auto Platform = Cast<APlatformBase>(Actor); IsValid(Platform))
		{
			if (const FVector CurrentScale = Platform->GetActorScale3D(); CurrentScale.Y > Platform->MinWidth)
			{
				const float NewYScale = CurrentScale.Y - DecreaseValue;
				Platform->SetActorScale3D(FVector(Platform->ScaleX, NewYScale, Platform->ScaleZ));
				const float CurrentCapsuleHalfHeight = Platform->CapsuleComponent->GetUnscaledCapsuleHalfHeight();
				const float NewCapsuleHalfHeight = CurrentCapsuleHalfHeight - 4;
				Platform->CapsuleComponent->SetCapsuleHalfHeight(NewCapsuleHalfHeight);
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red,
													 FString::Format(
														 *FString(TEXT("Decrease Platform Size, now is: {0}")),
														 {Platform->GetActorScale3D().Y}));

					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red,
													 FString::Format(
														 *FString(TEXT("Decrease Capsule Size, now is: {0}")),
														 {Platform->CapsuleComponent->GetComponentScale().Y}));
				}
			}
		}
	}
}

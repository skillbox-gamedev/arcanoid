// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "SpawnAdditionalBall.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API ASpawnAdditionalBall : public ABonusBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly)
	double IncreaseValue;
	
	void Apply(AActor* Actor) override;
};

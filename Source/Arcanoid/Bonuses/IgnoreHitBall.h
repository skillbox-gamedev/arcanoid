// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "IgnoreHitBall.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AIgnoreHitBall : public ABonusBase
{
	GENERATED_BODY()

public:
	AIgnoreHitBall();

	UPROPERTY(EditDefaultsOnly)
	float TimerRate;
	
	UPROPERTY()
	FTimerHandle BonusTimerHandler;
	
	void Apply(AActor* Actor) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "PlatformBase.generated.h"

UCLASS()
class ARCANOID_API APlatformBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
	UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinWidth;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxWidth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpeed;
	
	UPROPERTY(VisibleAnywhere)
	float ScaleX;

	UPROPERTY(VisibleAnywhere)
	float ScaleZ;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};

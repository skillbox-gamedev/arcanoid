// Fill out your copyright notice in the Description page of Project Settings.


#include "DecreaseBallSpeed.h"

#include "Arcanoid/Base/BallBase.h"
#include "Kismet/GameplayStatics.h"

ADecreaseBallSpeed::ADecreaseBallSpeed()
{
	DecreaseValue = 100.0f;
}

void ADecreaseBallSpeed::Apply(AActor* Actor)
{
	if (IsValid(Actor))
	{
		TArray<AActor*> FoundBalls;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABallBase::StaticClass(), FoundBalls);
		for (AActor* BallActor : FoundBalls)
		{
			if (ABallBase* Ball = Cast<ABallBase>(BallActor); IsValid(Ball))
			{
				float BallSpeed = Ball->Speed;

				if (BallSpeed > Ball->MinBallSpeed)
				{
					BallSpeed -= DecreaseValue;
				}
				Ball->Speed = BallSpeed;
				if (GEngine)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red,
													 FString::Format(*FString(TEXT("Decrease Ball Speed, now is: {0}")),
																	 {Ball->Speed}));
				}
			}
		}
	}
}
